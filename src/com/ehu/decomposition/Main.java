package com.ehu.decomposition;

public class Main {
    public static void main(String[] args) {
    }

    public static int findGreatestCommonDivisor(final int firstNumber, final int secondNumber) {
        if (secondNumber == 0) {
            return firstNumber;
        }
        return findGreatestCommonDivisor(secondNumber, firstNumber % secondNumber);
    }

    public static int findGreatestCommonDivisorForFourNumbers(final int a, final int b, final int c, final int d) {
        return findGreatestCommonDivisor(findGreatestCommonDivisor(a, b), findGreatestCommonDivisor(c, d));
    }

    public static int findSmallestCommonMultiple(final int firstNumber, final int secondNumber) {
        return firstNumber * secondNumber / findGreatestCommonDivisor(firstNumber, secondNumber);
    }

    public static int findSecondLargestNumber(final int[] inputNumbers) {

        int[] numbers = new int[inputNumbers.length];
        System.arraycopy(inputNumbers,0, numbers, 0, inputNumbers.length);

        boolean isSorted = false;
        int temp;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < numbers.length - 1; i++) {
                if (numbers[i] < numbers[i + 1]) {
                    isSorted = false;
                    temp = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = temp;
                }
            }
        }
        return numbers[1];
    }

    public static int findFactorial(final int number) {
        if (number <= 0) {
            return 1;
        }

        return number * findFactorial(number - 1);
    }

    public static int findSumOfFactorials() {
        int factorialsSum = 0;
        for (int i = 1; i < 10; i++) {
            if (i % 2 != 0)
                factorialsSum += findFactorial(i);
        }
        return factorialsSum;
    }
}
